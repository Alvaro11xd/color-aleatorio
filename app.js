// Seleccionar los elementos del DOM
const boton = document.querySelector("button");
const color = document.getElementById("color");

import { generarColorHexAleatorio } from "./functions.js";

boton.addEventListener("click", () => {
  let colorAleatorio = generarColorHexAleatorio();
  //   Actualizar el texto
  color.textContent = colorAleatorio;
  //   Actualizar el color de fondo
  document.body.style.backgroundColor = colorAleatorio;
});
